import requests, json, shutil, random

finalId = 32796  # 17268

def getWaifuData():
    waifuId = random.randint(0, finalId)
    print(waifuId)
    r = requests.get(
        "https://mywaifulist.moe/api/waifu/" + str(waifuId),
        headers={"X-requested-with": "XMLHttpRequest"},
        verify=r"C:\Users\5h4d0w\AppData\Local\.certifi\cacert.pem"
    )

    return r, waifuId

def waifu_func():
    try:
        waifu = {}
        waifuData, waifuId = getWaifuData()
        if waifuData.headers["Content-Type"] == "application/json":
            waifu = json.loads(waifuData.text)['data']
            waifu_name = waifu['name']
            waifu_picture = waifu['display_picture']
            if waifu_picture == "":
                waifu_picture = waifu['appearances']['display_picture']

            #json.dump(waifu, open('waifu.json', 'w+'), indent=4, sort_keys=True)

            print(f'[]({waifu_picture})')
    except Exception as e:
        print(e)

def main():
    waifu_func()

if __name__ == '__main__':
    main()