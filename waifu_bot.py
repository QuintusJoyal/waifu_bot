import requests, json, random, logging, os
from uuid import uuid4
from telegram import (
    Update,
    ForceReply,
    ParseMode,
    InlineQueryResultArticle,
    InputTextMessageContent,
)
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    InlineQueryHandler,
    Filters,
    CallbackContext,
)

finalId = 32796

PORT = int(os.environ.get("PORT", "8443"))

TOKEN = ""

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.DEBUG,
)

logger = logging.getLogger(__name__)


def start(update: Update, context: CallbackContext):
    user = update.effective_user
    update.message.reply_markdown_v2(
        fr"Hello There {user.mention_markdown_v2()}\!",
        reply_markup=ForceReply(selective=True),
    )


def bot_help(update: Update, context: CallbackContext):
    update.message.reply_text("/waifu - Get your waifu!")


def getWaifuData():
    waifuId = random.randint(0, finalId)
    try:
        r = requests.get(
            "https://mywaifulist.moe/api/waifu/" + str(waifuId),
            headers={"X-requested-with": "XMLHttpRequest"},
        )
    except Exception as e:
        print(e)

    return r, waifuId


def waifu_func():
    try:
        waifu = {}
        waifuData, waifuId = getWaifuData()
        if waifuData.headers["Content-Type"] == "application/json":
            waifu = json.loads(waifuData.text)["data"]
            waifu_name = waifu["name"]
            waifu_picture = waifu["display_picture"]
            if waifu_picture == "":
                waifu_picture = waifu["appearances"]["display_picture"]

            waifu_description = waifu["description"]
            waifu_series = waifu["series"]["name"]

            return '<a href="{0}">Waifu</a>\n<b>Name</b>: {1}\n\n<b>Description</b>: \n{2}\n\n<b>Series</b>: {3}'.format(
                waifu_picture, waifu_name, waifu_description, waifu_series
            )
    except Exception as e:
        print(e)
    
    return "Out of 30000+ waifus you got None"


def replyTo(update: Update, context: CallbackContext):

    waifu_msg = str(waifu_func())

    update.message.reply_text(waifu_msg, parse_mode=ParseMode.HTML)


def inlineQuery(update: Update, context: CallbackContext):

    query = update.inline_query.query

    if query == "waifu":
        waifu_msg = str(waifu_func())

        result = []

        result.append(
            InlineQueryResultArticle(
                id=str(uuid4()),
                title="Waifu",
                description="Click To get your random waifu",
                thumb_url="https://www.dictionary.com/e/wp-content/uploads/2018/03/waifu.jpg",
                input_message_content=InputTextMessageContent(
                    waifu_msg, parse_mode=ParseMode.HTML
                ),
            )
        )

        update.inline_query.answer(result, cache_time=5)
    else:
        return


def main():
    updater = Updater(TOKEN)
    dispat = updater.dispatcher
    dispat.add_handler(CommandHandler("start", start))
    dispat.add_handler(CommandHandler("help", bot_help))
    dispat.add_handler(CommandHandler("waifu", replyTo))

    inlineQ = InlineQueryHandler(inlineQuery)

    dispat.add_handler(inlineQ)

    updater.start_webhook(
        listen="0.0.0.0",
        port=PORT,
        url_path=TOKEN,
        webhook_url="" + TOKEN,
    )

    updater.idle()


if __name__ == "__main__":
    main()
